# Device Management API specification

This repository shows the API Spec for the Device Management API. 

The generated documentation links can be found here:

## Swagger
- Stable (Main Branch) https://nunet.gitlab.io/open-api/device-management-api-spec/main/swagger/
- Beta (Staging Branch) https://nunet.gitlab.io/open-api/device-management-api-spec/staging/swagger/
- Alpha (Develop Branch) https://nunet.gitlab.io/open-api/device-management-api-spec/develop/swagger/

## Async API
- Stable (Main Branch) https://nunet.gitlab.io/open-api/device-management-api-spec/main/async-api/
- Beta (Staging Branch) https://nunet.gitlab.io/open-api/device-management-api-spec/staging/async-api/
- Alpha (Develop Branch) https://nunet.gitlab.io/open-api/device-management-api-spec/develop/async-api/

## IPLD Schema
Prerequisites
- We assumes that your machine has installed `node` and `npm` for running the validation script 

After the installation here's a step to Run the validation script for device management api:
1. **Install Dependencies**:
    Download all dependencies for validation script using npm

```
npm install
```

2. **Run the Script**:
    Run the Validation script using node

```
node validator.js
```

It will run for each of api desribed in `.ipldsch` if schema is valid the result will looks like this

```
typeName:  OnboardRequest
Object data for type OnboardRequest is valid!

``` 