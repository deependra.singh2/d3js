    // api url
    const apiURLStacked = "http://127.0.0.1:8008/st/"
    const apiURL = "http://127.0.0.1:8008/js/"
    var data =[]
    var _height = 300
    var _height_g = 600
    var graphType = ["line"] 
    var graphType = ["stacked"] 
    var graphType = ["stacked", "line"] 

    var debug = false

    function apiCallback(apiResponseData) {
        if (apiResponseData === null){
            apiResponseData = 
                [
                    [
                        4,
                        11
                    ],
                    [
                        5,
                        111
                    ],
                    [
                        21,
                        176
                    ],
                    [
                        23,
                        203
                    ],
                    [
                        51,
                        205
                    ],
                    [
                        55,
                        206
                    ],
                    [
                        68,
                        242
                    ],
                    [
                        73,
                        247
                    ],
                    [
                        80,
                        253
                    ],
                    [
                        85,
                        255
                    ]
                ]
        }
        return _mainRenderer(apiResponseData)
    }

    function _mainRenderer(_nums) {
        var datasetLine = _nums
        if (debug){
            console.log("--------------Started Rendering------------")
            console.log("--------------Consoling Data------------")
            console.log(datasetLine)
            console.log(data)
        }
        
        const color = ["green", "yellow", "red",];
        // Create SVG and padding for the chart
        const svg = d3
            .select("#chart")
            .style("height", 600)    
            .append("svg")
            .attr("height", _height_g)
            .attr("width", 800)
            .style("padding-top", 30)    
            .attr('id', 'main-svg');

        const strokeWidth = 0;
        const margin = { top: 0, bottom: 20, left: 30, right: 20 };
        const chart = svg.append("g").attr("transform", `translate(${margin.left+40},0)`).attr('id', 'main-g');

        const width = +svg.attr("width") - margin.left-150 - margin.right - strokeWidth * 2;
        const height = +svg.attr("height") - margin.top - margin.bottom-100;
        const grp = chart
            .append("g")
            .attr("transform", `translate(-${margin.left - strokeWidth},-${margin.top})`)
            .attr('id', 'x-main-g');

        // Create stack
        const stack = d3.stack().keys(["stack_1", "stack_2", "stack_3"]);
        const stackedValues = stack(data);
        const stackData = [];
        // Copy the stack offsets back into the data.
        stackedValues.forEach((layer, index) => {
            const currentStack = [];
            layer.forEach((d, i) => {
                currentStack.push({
                    values: d,
                    year: data[i].year
                });
            });
            stackData.push(currentStack);
        });

        // Create scales
        const yScale = d3
            .scaleLinear()
            .range([height, 0])
            .domain([0, 102]);

        const yScaleRight = d3
            .scaleLinear()
            .range([height, 0])
            .domain([0, 102])

        const xScale = d3
            .scaleLinear()
            .range([0, width])
            .domain([0,100]);

        const area = d3
            .area()
            .x(dataPoint => xScale(dataPoint.year))
            .y0(dataPoint => yScale(dataPoint.values[0]))
            .y1(dataPoint => yScale(dataPoint.values[1]));

        const series = grp
            .selectAll(".series")
            .data(stackData)
            .enter()
            .append("g")
            .attr("class", "series");

        if (graphType.includes('stacked')){
            series
                .append("path")
                .attr("transform", `translate(${margin.left},0)`)
                .style("fill", (d, i) => color[i])
                .attr("stroke", "black")
                .attr("stroke-linejoin", "square")
                .attr("stroke-linecap", "square")
                .attr("stroke-width", 0)
                .attr("d", d => area(d));
            };
        // Add the X Axis
        chart
            .append("g")
            .attr("transform", `translate(0,${height})`)
            .call(d3.axisBottom(xScale).ticks(data.length));

        // Add the Y Axis
        chart
            .append("g")
            .attr("transform", `translate(0, 0)`)
            .call(d3.axisLeft(yScale).tickSizeOuter(0));

        chart
            .append("g")
            .attr("transform", "translate(" + width + " ,0)")
            .call(d3.axisRight(yScaleRight).tickSizeOuter(0));

        chart
            .append("text")
            .attr("transform", "rotate(-90)")
            .attr("y", 0 - margin.left-40)
            .attr("x",0 - (height / 2))
            .attr("dy", "1em")
            .style("text-anchor", "middle")
            .attr("font-size", 20)
            .text("% Buffer Consumed");             

        chart
            .append("text")
            .attr("text-anchor", "middle")
            .attr("y", height+60)
            .attr("x", (width/2))
            .attr("dx", "1em")
            .attr("font-size", 20)
            .text("% Critical Chain Completed");     
        
        d3.select("#main-svg")
            .style("padding-left", 40)
            .style("padding-top", 40);

        if (graphType.includes('line')){
            // Draw dots
            d3.select("#main-g")
                .append('g')
                .selectAll("dot")
                .data(datasetLine)
                .enter()
                .append("circle")
                .attr("cx", function (d) { return xScale(d[0]); })
                .attr("cy", function (d) { return yScale(d[1]); })
                .attr("r", 3)
                .attr("transform", `translate(${margin.left-30},0)`)
                .style("fill", "#000000");

            // Draw Lines
            var line = d3.line()
                .x(function (d) { return xScale(d[0]); })
                .y(function (d) { return yScale(d[1]); })
                .curve(d3.curveMonotoneX)

            // Render Lines
            series
                .append("path")
                .datum(datasetLine)
                .attr("class", "line")
                .attr("transform", `translate(${margin.left},0)`)
                .attr("d", line)
                .style("fill", "none")
                .style("stroke", "black")
                .style("stroke-width", "1");
        } else { console.log("----Not adding Line-----")}
    };
    function saveSvg() {
        saveSvgAsPng(d3.select("svg").node(), 'fever-cahrt.png',{backgroundColor:"white"});
    }

    // Function to set Stack area data
    function setStackedData(nums){
        console.log("---Setting Stack Data----")
        data = nums
        if(nums == null || nums == undefined){
            data = [
                {year: 0, stack_1: 0, stack_2: 0, stack_3: 110},
                {year: 10, stack_1: 10, stack_2: 3, stack_3: 100},
                {year: 20, stack_1: 20, stack_2: 6, stack_3: 90},
                {year: 30, stack_1: 30, stack_2: 9, stack_3: 80},
                {year: 40, stack_1: 40, stack_2: 12, stack_3: 70},
                {year: 50, stack_1: 50, stack_2: 15, stack_3: 60},
                {year: 60, stack_1: 60, stack_2: 18, stack_3: 50},
                {year: 70, stack_1: 70, stack_2: 21, stack_3: 40},
                {year: 80, stack_1: 80, stack_2: 24, stack_3: 30},
                {year: 90, stack_1: 90, stack_2: 27, stack_3: 20},
                {year: 100, stack_1: 100, stack_2: 30, stack_3: 10}
            ]
        }
        if(graphType == "stacked"){
            return apiCallback(nums)
        }
        readAPI(apiURL, apiCallback)
    }

    // Fetch Data from API and invoke the callback function
    function readAPI(url, callbackFunction) {
        d3.request(url)
            .mimeType("application/json")
            .response(function (xhr) { 
                return JSON.parse(xhr.responseText); })
            .get(callbackFunction);
    };


    function main(){
            readAPI(apiURLStacked, setStackedData)
    }
    console.log("-------------Drawing--", graphType, "--Graph---------------------")
    main()
